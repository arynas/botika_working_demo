from __future__ import division
from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier
import csv
import re
import matplotlib.pyplot as plt

# Buat array kosong untuk tempat tweets
tweets = []


# Fungsi helper untuk menghilangkan semua non ASCII characters
# Hanya menggunakan string dengan ASCII characters.
def strip_non_ascii(string):
    ''' Returns the string without non ASCII characters'''
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)



# Load dan bersihkan data

with open('data.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    reader.next()
    for row in reader:

        tweet= dict()
        tweet['orig'] = row[0]

        # Abaikan retweets tweet
        if re.match(r'^RT.*', tweet['orig']):
            continue

        tweet['clean'] = tweet['orig']

        # Hilangkan all non-ascii characters
        tweet['clean'] = strip_non_ascii(tweet['clean'])

        # Normalize case
        tweet['clean'] = tweet['clean'].lower()

        # Hilangkan URLS.
        tweet['clean'] = re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', tweet['clean'])

        # Fix classic tweet lingo
        tweet['clean'] = re.sub(r'\bthats\b', 'that is', tweet['clean'])
        tweet['clean'] = re.sub(r'\bive\b', 'i have', tweet['clean'])
        tweet['clean'] = re.sub(r'\bim\b', 'i am', tweet['clean'])
        tweet['clean'] = re.sub(r'\bya\b', 'yeah', tweet['clean'])
        tweet['clean'] = re.sub(r'\bcant\b', 'can not', tweet['clean'])
        tweet['clean'] = re.sub(r'\bwont\b', 'will not', tweet['clean'])
        tweet['clean'] = re.sub(r'\bid\b', 'i would', tweet['clean'])
        tweet['clean'] = re.sub(r'wtf', 'what the fuck', tweet['clean'])
        tweet['clean'] = re.sub(r'\bwth\b', 'what the hell', tweet['clean'])
        tweet['clean'] = re.sub(r'\br\b', 'are', tweet['clean'])
        tweet['clean'] = re.sub(r'\bu\b', 'you', tweet['clean'])
        tweet['clean'] = re.sub(r'\bk\b', 'OK', tweet['clean'])
        tweet['clean'] = re.sub(r'\bsux\b', 'sucks', tweet['clean'])
        tweet['clean'] = re.sub(r'\bno+\b', 'no', tweet['clean'])
        tweet['clean'] = re.sub(r'\bcoo+\b', 'cool', tweet['clean'])

        # Hilangkan Emoticons
        tweet['clean'] = re.sub(r'\b:\)\b', 'good', tweet['clean'])
        tweet['clean'] = re.sub(r'\b:D\b', 'good', tweet['clean'])
        tweet['clean'] = re.sub(r'\b:\(\b', 'sad', tweet['clean'])
        tweet['clean'] = re.sub(r'\b:-\)\b', 'good', tweet['clean'])
        tweet['clean'] = re.sub(r'\b=\)\b', 'good', tweet['clean'])
        tweet['clean'] = re.sub(r'\b\(:\b', 'good', tweet['clean'])
        tweet['clean'] = re.sub(r'\b:\\\b', 'annoyed', tweet['clean'])

        tweets.append(tweet['clean'])

# Membagi dataset menjadi data training dan data test dengan pembagian 80:20
new_train_tweet, new_test_tweet = tweets[0:684], tweets[685:855]

# Memisahkan data training yang mempunyai sentimen positif, negatif, dan netral
def separate_sentiment_train():
    train_tweets = []
    for tweet in new_train_tweet:
        blob = TextBlob(tweet)
        if blob.sentiment.polarity < 0:         #Negative
            train_tweets.append((tweet, 'negative'))
        elif blob.sentiment.polarity == 0:      #Neutral
            train_tweets.append((tweet, 'neutral'))
        else:                                   #Positive
            train_tweets.append((tweet, 'positive'))
    return train_tweets

# Melakukan proses training yang akan menghasilkan model dengan Naive Bayes Classifier
model = NaiveBayesClassifier(separate_sentiment_train())

# Test model dengan memakai data test. Jika hasil test Positive, masukan ke array Positive
# Jika hasil test Neutral, masukan ke array Neutral. Jika hasil test Negative, masukan ke array Negative
positive = []
negative = []
neutral = []
for tweet in new_test_tweet:
    if model.classify(tweet) == 'positive':
        positive.append(model.classify(tweet))
    elif model.classify(tweet) == 'negative':
        negative.append(model.classify(tweet))
    else:
        neutral.append(model.classify(tweet))

pos = len(positive)
neg = len(negative)
neu = len(neutral)

# Cetak hasil sentimen
print("Sentimen Positif: {}%".format(pos*100/len(new_test_tweet)))
print("Sentimen Netral: {}%".format(neu*100/len(new_test_tweet)))
print("Sentimen Negatif: {}%".format(neg*100/len(new_test_tweet)))

# Buat pie chart dari hasil sentimen
labels = 'Positive', 'Neutral', 'Negative'
sizes = [pos, neu, neg]
colors = ['yellowgreen', 'gold', 'lightcoral']
plt.pie(sizes, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=90)
plt.axis('equal')
plt.show()